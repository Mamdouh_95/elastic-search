<?php

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'size' => $faker->numberBetween(1, 100),
        'user_id' => 1
    ];
});
