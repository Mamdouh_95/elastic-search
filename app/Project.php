<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    const ELASTIC_INDEX = 'elastic-app';
    const ELASTIC_TYPE = 'project';
    protected $fillable = ['name', 'size', 'user_id'];
}
