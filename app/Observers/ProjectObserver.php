<?php

namespace App\Observers;

use App\Project;
use Elasticsearch\ClientBuilder;

class ProjectObserver
{
    /**
     * Handle the project "created" event.
     *
     * @param  \App\Project  $project
     * @return void
     */
    public function created(Project $project)
    {
        $client = ClientBuilder::create()->build();
        $client->index([
            'index' => Project::ELASTIC_INDEX,
            'type' => Project::ELASTIC_TYPE,
            'id' => $project->id,
            'body' => [
                'name' => $project->name,
                'size' => $project->size,
                'user_id' => $project->user_id
            ]
        ]);
    }

    /**
     * Handle the project "updated" event.
     *
     * @param  \App\Project  $project
     * @return void
     */
    public function updated(Project $project)
    {
        //
    }

    /**
     * Handle the project "deleted" event.
     *
     * @param  \App\Project  $project
     * @return void
     */
    public function deleted(Project $project)
    {
        $client = ClientBuilder::create()->build();
        $client->delete([
            'index' => Project::ELASTIC_INDEX,
            'type' => Project::ELASTIC_TYPE,
            'id' => $project->id,
        ]);
    }

    /**
     * Handle the project "restored" event.
     *
     * @param  \App\Project  $project
     * @return void
     */
    public function restored(Project $project)
    {
        //
    }

    /**
     * Handle the project "force deleted" event.
     *
     * @param  \App\Project  $project
     * @return void
     */
    public function forceDeleted(Project $project)
    {
        //
    }
}
