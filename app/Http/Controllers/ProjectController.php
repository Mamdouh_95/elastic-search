<?php

namespace App\Http\Controllers;

use App\Project;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    // Normal Search
    public function search(Request $request)
    {
        $name = $request->name;
        dd($name);
    }

    // Elastic Search
    public function elastic(Request $request)
    {
        $name = $request->name;

        $results = \Elasticsearch::search([
            'index' => Project::ELASTIC_INDEX,
            'type' => Project::ELASTIC_TYPE,
            'sort' => [
                '_score'
            ],
            'body' => [
//                'query' => [],
            ],
        ]);

        $projects = array_map(function ($result){
            $sourceProject = $result['_source'];
            $project = new Project([
               'name' => $sourceProject['name'],
               'size' => $sourceProject['size'],
               'user_id' => $sourceProject['user_id']
            ]);
            return $project;
        }, $results['hits']['hits']);

        dd($results['hits']);
    }
}
